import React, {Component} from 'react'
import {browserHistory} from 'react-router'
import Loading from '../compontents/Loading.js'
import service from '../services/movieService.js'

import '../styles/movieList.css'

export default class MovieSearchContainer extends Component {
    constructor(props) {
        super(props)
        this.state = {
            // 是否显示遮罩
            isLoading:true,
            isLoadMore:false,
            // 要渲染的电影列表数据
            movieListData:[],
            // 分页信息
            message:{
                pageIndex:1,
                start:0,
                count:10,
                keyword:this.props.params.keyword
            }
        }
    }

    componentDidMount(){
        this.fetch(this.state.message.keyword)
    }

    componentWillReceiveProps(nextProps){
        this.fetch(nextProps.params.keyword)
    }

    componentDidUpdate(){
        if(this.state.isLoading){
            this.fetch(this.state.message.keyword)
        }else {
            this.addScrollEvent()
        }
    }

    // 添加列表的滚动事件
    addScrollEvent=()=>{
        const _this=this
        this.refs.scrollContainer.onscroll=function (e) {
            if(e.target.scrollHeight==e.target.scrollTop+e.target.offsetHeight){

                // 防止手贱多次请求数据
                if(_this.state.isLoadMore){
                    return
                }

                _this.setState({
                    isLoadMore:true
                })
                _this.fetch(_this.state.message.keyword)
            }
        }
    }

    // 请求电影列表数据
    fetch=(keyword)=>{
        const _this=this

        if(keyword==undefined){
            return
        }

        if(keyword.trim()==''){
            return
        }

        // 分页信息重置操作
        if(keyword!=this.state.message.keyword){
            // 不是立即执行赋值
            this.setState({
                // 是否显示遮罩
                isLoading:true,
                isLoadMore:false,
                // 要渲染的电影列表数据
                movieListData:[],
                // 分页信息
                message:{
                    pageIndex:1,
                    start:0,
                    count:10,
                    keyword:keyword
                }
            })
            return
        }


        // 数组的深拷贝
        let movieListData=[].concat(this.state.movieListData)
        // 对象的深拷贝
        let message=Object.assign({},this.state.message)
        // 修改分页信息
        message.start=(message.pageIndex-1)*message.count
        message.pageIndex=message.pageIndex+1

        // 发起数据请求
        const promise=service.searchMovieListData(JSON.stringify(message))
        promise.then(function (data) {

            // 判断数组数据量
            if(movieListData.length>0){
                // 其他页码赋值
                movieListData=movieListData.concat(data.subjects)
            }else{
                // 第一页数据赋值
                movieListData=data.subjects
            }


            // 判断实例是否还存在
            if(_this._reactInternalInstance){
                // 从新赋值
                _this.setState({
                    isLoading:false,
                    isLoadMore:false,
                    movieListData:movieListData,
                    message:message
                })
            }

        },function (err) {

        })


    }

    // 跳转到详细页面
    goDetail=(id)=>{
        browserHistory.push(`/movie/movieDetail/${id}`)
    }

    // 渲染遮罩
    renderLoading=()=>{
        return (
            <Loading/>
        )
    }

    // 渲染电影列表
    renderMovieList=()=>{
        return (
            <div ref="scrollContainer" className="movieList_container">
                {this.state.movieListData.map(this.renderRow)}
                <div className={this.state.isLoadMore?'movieList_show':'movieList_hide'}>
                    <span>正在加载更多数据...</span>
                </div>
            </div>
        )
    }

    // 渲染列表每一行
    renderRow=(item)=>{
        return (
            <div onClick={()=>this.goDetail(item.id)} key={item.id+Math.random()} className="movieList_row">
                <div>
                    <img src={item.images.small} alt=""/>
                </div>
                <div>
                    <h1>{item.title}</h1>
                    <span>{item.year}</span>
                </div>
            </div>
        )
    }



    render() {
        if(this.state.isLoading){
            return this.renderLoading()
        }else{
            return this.renderMovieList()
        }
    }
}
