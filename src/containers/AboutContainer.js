
import React, { Component } from 'react'
import { message as Message,Form, Input,Button } from 'antd'
const FormItem = Form.Item
import service from '../services/aboutService.js'
import '../styles/about.css'

class AboutContainer extends Component {
    constructor(props) {
        super(props)
        this.state={
        }
    }

    handleSubmit=(e)=> {
        e.preventDefault()
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                this.sendFeedback(values)
            }
        })
    }

    handleReset = () => {
        this.props.form.resetFields()
    }

    sendFeedback=(message)=>{
        var _this=this
        const promise=service.sendFeedBack(JSON.stringify(message))
        promise.then(
            function (data) {
                if(data.status=='OK'){
                    _this.handleReset()
                    Message.success('您的意见反馈我们已经收到，请耐心等待问题的解决！')
                }
            },
            function (err) {
            }
        ).catch(function (err) {

        })
    }


    render() {
        const { getFieldDecorator } = this.props.form
        const formItemLayout = {
            labelCol: { span: 6 },
            wrapperCol: { span: 14 },
        }
        const tailFormItemLayout = {
            wrapperCol: {
                span: 14,
                offset: 6,
            },
        }
        const prefixSelector = getFieldDecorator('prefix', {
            initialValue: '86',
        })(
            <span>+86</span>
        )

        return (
            <div className="about_container">
                <Form onSubmit={this.handleSubmit}>
                    <FormItem
                        {...formItemLayout}
                        label={(
                            <span>
                              意见反馈
                            </span>
                        )}
                        hasFeedback
                    >
                        {getFieldDecorator('feedback', {
                            rules: [{ required: true, message: '请输入您的反馈信息' }],
                        })(
                            <Input />
                        )}
                    </FormItem>
                    <FormItem
                        {...formItemLayout}
                        label={(
                            <span>
                              联系姓名
                            </span>
                        )}
                        hasFeedback
                    >
                        {getFieldDecorator('name', {
                            rules: [{ required: true, message: '请输入您的姓名' }],
                        })(
                            <Input />
                        )}
                    </FormItem>

                    <FormItem
                        {...formItemLayout}
                        label="联系电话"
                    >
                        {getFieldDecorator('phone', {
                            rules: [{ pattern: /^[0-9]{11}$/,required: true, message: '请输入您的电话' }],
                        })(
                            <Input addonBefore={prefixSelector}/>
                        )}
                    </FormItem>

                    <FormItem {...tailFormItemLayout}>
                        <Button type="primary" htmlType="submit" size="large">提交</Button>
                    </FormItem>
                </Form>
            </div>
        )
    }
}
export default Form.create()(AboutContainer)
