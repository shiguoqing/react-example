import React, {Component} from 'react'
import {Link} from 'react-router'

import '../styles/movie.css'
export default class MovieContainer extends Component {
    constructor(props) {
        super(props)
        this.state = {
            movieType:'in_theaters',
            keyword:''
        }
    }

    // 利用context特性获取react-router中的router对象
    static contextTypes={
        router: React.PropTypes.object
    }

    // 跳转搜索页面
    goSearch=(keyword)=>{
        if(keyword==''){
            return
        }

        this.context.router.push(`/movie/movieSearch/${this.state.keyword}`)
        this.setState({
            movieType:'',
            keyword:''
        })
    }

    // 改变电影类型
    changeMovieType=(movieType)=>{
        this.setState({
            movieType
        })
    }

    // 改变输入框关键字
    changeKeyword=(e)=>{
        this.setState({
            keyword:e.target.value
        })
    }

    render() {
        return (
            <div className="movie_container">
                <div className="movie_left">
                    <Link onClick={()=>this.changeMovieType('in_theaters')} className={this.state.movieType=='in_theaters'?'movie_current':''} to="/movie/movieList/in_theaters">正在热映</Link>
                    <Link onClick={()=>this.changeMovieType('coming_soon')} className={this.state.movieType=='coming_soon'?'movie_current':''} to="/movie/movieList/coming_soon">即将上映</Link>
                    <Link onClick={()=>this.changeMovieType('top250')} className={this.state.movieType=='top250'?'movie_current':''} to="/movie/movieList/top250">top250</Link>
                </div>
                <div className="movie_right">
                    <div className="movie_search">
                        <input onChange={this.changeKeyword} value={this.state.keyword} type="text"/>
                        <button onClick={()=>this.goSearch(this.state.keyword)}>搜索</button>
                    </div>
                    <div className="movie_content">
                        {this.props.children}
                    </div>
                </div>
            </div>
        )
    }
}