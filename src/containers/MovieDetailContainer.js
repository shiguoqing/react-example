import React, {Component} from 'react'
import {browserHistory} from 'react-router'
import Loading from '../compontents/Loading.js'
import service from '../services/movieService.js'

import '../styles/movieDetail.css'

export default class MovieDetailContainer extends Component {
    constructor(props) {
        super(props)
        this.state = {
            // 是否显示遮罩
            isLoading:true,
            // 要渲染的电影列表数据
            movieDetailData:{}
        }
    }

    componentDidMount(){
        this.fetch()
    }

    // 请求电影列表数据
    fetch=()=>{
        const _this=this

        const promise=service.getMovieDetailData(this.props.params.id)
        promise.then(function (data) {

            _this.setState({
                isLoading:false,
                movieDetailData:data
            })


        },function (err) {

        })


    }

    // 跳转到详细页面
    goDetail=(id)=>{
        browserHistory.push(`/movie/movieDetail/${id}`)
    }

    // 渲染遮罩
    renderLoading=()=>{
        return (
            <Loading/>
        )
    }

    // 渲染电影列表
    renderMovieDetail=()=>{
        return (
            <div className="movieDetail_container">
                <div>
                    <img src={this.state.movieDetailData.images.large} alt=""/>
                </div>
                <div>
                    <h1>{this.state.movieDetailData.title}</h1>
                    <p>{this.state.movieDetailData.summary}</p>
                </div>
            </div>
        )
    }


    render() {
        if(this.state.isLoading){
            return this.renderLoading()
        }else{
            return this.renderMovieDetail()
        }
    }
}
