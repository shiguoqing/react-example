import React, {Component} from 'react'
import {Router, Route, IndexRoute, Redirect, hashHistory,browserHistory} from 'react-router'

import AppContainer from '../containers/AppContainer.js'
import HomeContainer from '../containers/HomeContainer.js'
// 异步加载原则，所有首页不用的东西，全都异步加载
// import MovieContainer from '../containers/MovieContainer.js'
// import AboutContainer from '../containers/AboutContainer.js'
// import MovieListContainer from '../containers/MovieListContainer.js'
// import MovieDetailContainer from '../containers/MovieDetailContainer.js'
// import MovieSearchContainer from '../containers/MovieSearchContainer.js'
//

export default class Routers extends Component {
    render() {
        return (
            <Router history={browserHistory}>
                <Route path="/" component={AppContainer}>
                    <IndexRoute component={HomeContainer}/>
                    <Route path="home" component={HomeContainer}/>
                    <Route path="movie"
                           getComponent={
                               (nextState, callback)=> {
                                   require.ensure([], (require)=> {
                                       callback(null, require("../containers/MovieContainer.js").default)
                                   }, "movie")
                               }
                           }
                           onEnter={()=>false}
                           onLeave={()=>false}
                    >
                        <IndexRoute getComponent={
                            (nextState, callback)=> {
                                require.ensure([], (require)=> {
                                    callback(null, require("../containers/MovieListContainer.js").default)
                                }, "movieList")
                            }
                        }
                        />
                        <Route path="movieList/:movieType"
                               getComponent={
                                   (nextState, callback)=> {
                                       require.ensure([], (require)=> {
                                           callback(null, require("../containers/MovieListContainer.js").default)
                                       }, "movieList")
                                   }
                               }
                        />
                        <Route path="movieDetail/:id"
                               getComponent={
                                   (nextState, callback)=> {
                                       require.ensure([], (require)=> {
                                           callback(null, require("../containers/MovieDetailContainer.js").default)
                                       }, "movieDetail")
                                   }
                               }
                        />
                        <Route path="movieSearch(/:keyword)"
                               getComponent={
                                   (nextState, callback)=> {
                                       require.ensure([], (require)=> {
                                           callback(null, require("../containers/MovieSearchContainer.js").default)
                                       }, "movieSearch")
                                   }
                               }
                        />
                        {/*<Redirect from="movieList" to="/movieList"/>*/}
                    </Route>
                    <Route path="About"
                           getComponent={
                               (nextState, callback)=> {
                                   require.ensure([], (require)=> {
                                       callback(null, require("../containers/AboutContainer.js").default)
                                   }, "about")
                               }
                           }
                    />
                </Route>
            </Router>
        )
    }
}
